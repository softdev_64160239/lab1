/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;
import java.util.Scanner;
/**
 *
 * @author chanatip
 */
public class Lab1 {
    private char[][] board;
    private char currentPlayer;

    public Lab1() {
        System.out.println("WELCOME TO OX GAME");

        board = new char[3][3];
        currentPlayer = 'X';

        
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                board[row][col] = ' ';
            }
        }
    }
    public void printBoard() {
        System.out.println("---------");
        for (int row = 0; row < 3; row++) {
            System.out.print("|");
            for (int col = 0; col < 3; col++) {
                System.out.print(board[row][col] + "|");
            }
            System.out.println("\n---------");
        }
    }

    public boolean checkWin() {

        for (int i = 0; i < 3; i++) {
            if (board[i][0] == currentPlayer && board[i][1] == currentPlayer && board[i][2] == currentPlayer) {
                return true;
            }

            if (board[0][i] == currentPlayer && board[1][i] == currentPlayer && board[2][i] == currentPlayer) {
                return true;
            }
        }

 
        if (board[0][0] == currentPlayer && board[1][1] == currentPlayer && board[2][2] == currentPlayer) {
            return true;
        }

        if (board[0][2] == currentPlayer && board[1][1] == currentPlayer && board[2][0] == currentPlayer) {
            return true;
        }

        return false;
    }

    public boolean isBoardFull() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (board[row][col] == ' ') {
                    return false;
                }
            }
        }
        return true;
    }

    public void playGame() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            printBoard();
            System.out.print("Player " + currentPlayer + ", enter your move (row col): ");
            String input = scanner.nextLine();
            
            if(input.matches("\\d\\d")){

            int row = Character.getNumericValue(input.charAt(0));
            int col = Character.getNumericValue(input.charAt(1));

            if (row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == ' ') {
                board[row][col] = currentPlayer;

                if (checkWin()) {
                    printBoard();
                    System.out.println("Player " + currentPlayer + " wins!");
                    break;
                }

                if (isBoardFull()) {
                    printBoard();
                    System.out.println("It's a tie!");
                    break;
                }

                currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
            } else {
                System.out.println("Invalid move. Please try again.");
            }
            
            }else {
                System.out.println("Invalid input format. Please enter the position as 'row:col'.");
        }
            
        }

        scanner.close();
    }

    public static void main(String[] args) {
        Lab1 game = new Lab1();
        game.playGame();
    }
}
